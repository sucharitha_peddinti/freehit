package com.supernal.freehit

import android.util.Log
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

object Utility {
    private const val lottoPool = 70
    // return YYY/MM/DD
    val formattedDate:
            // Get Mingguo year
            String
        get() { // return YYY/MM/DD
            val cal = Calendar.getInstance()
            // Get Mingguo year
            val year = cal[Calendar.YEAR] - 1911
            val month = cal[Calendar.MONTH] + 1
            val date = cal[Calendar.DATE]
            return "$year/$month/$date"
        }

    fun createLottoTicket(): LottoTicket { // Create a empty millionaire list
        val millionaire = ArrayList<Int?>(8)
        // Define ArrayList to hold Integer objects
        val numbers = ArrayList<Int?>()
        for (i in 0 until lottoPool) {
            numbers.add(i + 1)
        }
        Collections.shuffle(numbers)
        for (j in 0..7) {
            millionaire.add(numbers[j])
        }
        val specialNumber = millionaire
            .removeAt(millionaire.size - 1)
        return LottoTicket(millionaire, specialNumber)
    }

    fun renderTicketIntoViewGroup(
        prize: LottoTicket,
        ticket: LottoTicket, lottoNormal: ViewGroup, lottoSpecial: ViewGroup
    ) {
        var count = 0
        // render Lotto number normal into layout
        for (i in 0 until lottoNormal.childCount) {
            val child = lottoNormal.getChildAt(i)
            if (child is TextView) {
                val tv = child
                val num = ticket.normalNumbers[count]!!.toInt()
                tv.text = num.toString() + ""
                if (prize.isMatchNormal(num)) {
                    tv.setBackgroundResource(R.drawable.choose_circle)
                }
                count++
            }
        }
        // render Lotto number special into layout
        for (i in 0 until lottoSpecial.childCount) {
            val child = lottoSpecial.getChildAt(i)
            if (child is TextView) {
                val tv = child
                val num = ticket.specialNember!!.toInt()
                tv.text = num.toString() + ""
                Log.e("utility", "" + num)
                if (prize.isMatchSpecial(num)) {
                    tv.setBackgroundResource(R.drawable.choose_circle)
                }
            }
        }
    }

    fun renderTicketIntoViewGroup(
        ticket: LottoTicket,
        lottoNormal: ViewGroup, lottoSpecial: ViewGroup
    ) {
        var count = 0
        // render Lotto number normal into layout
        for (i in 0 until lottoNormal.childCount) {
            val child = lottoNormal.getChildAt(i)
            if (child is TextView) {
                child.text = ticket.normalNumbers[count]
                    ?.toInt().toString() + ""
                Log.e("normal", "" + ticket.normalNumbers[count]!!.toInt())
                count++
            }
        }
        // render Lotto number special into layout
        for (i in 0 until lottoSpecial.childCount) {
            val child = lottoSpecial.getChildAt(i)
            if (child is TextView) {
                child.text = ticket.specialNember.toString() + ""
                count++
                Log.e("spl", "" + ticket.specialNember)
            }
        }
    }

    class LottoTicket(val normalNumbers: ArrayList<Int?>, val specialNember: Int?) {
        fun isBingo(newTicket: LottoTicket): Boolean {
            var isMatch = false
            for (num in newTicket.normalNumbers) {
                if (normalNumbers.contains(num)) {
                    isMatch = true
                }
            }
            return newTicket.specialNember === specialNember || isMatch
        }

        fun isMatchNormal(num: Int?): Boolean {
            return normalNumbers.contains(num)
        }

        fun isMatchSpecial(num: Int): Boolean {
            return num === specialNember
        }

    }
}