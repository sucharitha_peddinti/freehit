package com.supernal.freehit.Api

import com.supernal.freehit.Models.Luckynumcoupons_Model
import com.supernal.freehit.Models.Mycouponsresponse
import com.supernal.freehit.Models.SendCoupons_Model
import com.supernal.freehit.Models.User
import retrofit2.Call
import retrofit2.http.*

interface Api {

    //userlogin

    @FormUrlEncoded
    @POST("users/user_login/")
    fun createUser(
        @Field("mobile_number") mobilenumber: Long
    ): Call<User>


    //bussinesslogin

    @FormUrlEncoded
    @POST("users/business_login/")
    fun bussinesscreateUser(
        @Field("mobile_number") mobilenumber: Long
    ): Call<User>


    //sending coupon

    @FormUrlEncoded
    @POST("coupons/create/")
    fun sendcoupon(
        @Header("Authorization") authKey: String,
        @Field("send_to") mobilenumber: Long
    ): Call<SendCoupons_Model>


    //Get coupons
    @GET("coupons/my_coupons/")
    fun getcoupon(
        @Header("Authorization") authKey: String
    ): Call<Mycouponsresponse>

    //update lucky number
    @FormUrlEncoded
    @PATCH("coupons/update/1/")
    fun sendluckynumbers(
        @Header("Authorization") authKey: String,
        @Field("number") luckynumber: String
    ): Call<Luckynumcoupons_Model>


    //sending coupon status

    @POST
    fun sendingcouponresult(
        @Url url: String,
        @Header("Authorization") authKey: String
    ): Call<Luckynumcoupons_Model>


    //Get scratchedcoupons
    @GET("my_business_coupons")
    fun getscratchedcoupon(
        @Header("Authorization") authKey: String
    ): Call<Mycouponsresponse>

}

