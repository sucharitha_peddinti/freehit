package com.supernal.freehit.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

    /**
     * Created by sam  on 03/03/2020.
     */
    class APIClient {


        companion object {
//            private const val BASE_URL = "http://192.168.0.25:8000/api/v1/"
                    private const val BASE_URL = "http://ec2-13-232-66-148.ap-south-1.compute.amazonaws.com/api/v1/"

            var retofit: Retrofit? = null

            val client: Retrofit
                get() {

                    if (retofit == null) {
                        retofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                    }
                    return retofit!!
                }
        }
    }
