package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class Defaultcouponnumbers_Activity : Activity() {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var defaultluckynumbers: TextView
    lateinit var editnumbers: TextView
    lateinit var backarrow1: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.couponnumbers)
        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        var defaultnumbers = sharedPreferences.getString("defaultnumbers", "")

        defaultluckynumbers = findViewById<TextView>(R.id.defaultluckynumbers)
        backarrow1 = findViewById<ImageView>(R.id.backarrow1)
        editnumbers = findViewById<TextView>(R.id.editnumbers)

        defaultluckynumbers.text=""+defaultnumbers
        editnumbers.setOnClickListener {
            intent = Intent(applicationContext, NumberSeletion_Activity::class.java)
            startActivity(intent)

        }
        backarrow1.setOnClickListener{
            intent = Intent(applicationContext, Coupns_Activity::class.java)
            startActivity(intent)
        }
    }
}