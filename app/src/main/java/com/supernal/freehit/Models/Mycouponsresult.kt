package com.supernal.freehit.Models

import com.google.gson.annotations.SerializedName

data class Mycouponsresult (

    @SerializedName("id") val id : Int,
    @SerializedName("number") val number : String,
    @SerializedName("sent_to") val sent_to : Mycouponssentto,
    @SerializedName("coupon_id") val coupon_id : String,
    @SerializedName("scratch_date") val scratch_date : String
)