package com.supernal.freehit.Models

import com.google.gson.annotations.SerializedName

data class Mycouponssentto (

    @SerializedName("id") val id : Int,
    @SerializedName("first_name") val first_name : String,
    @SerializedName("date_of_birth") val date_of_birth : String,
    @SerializedName("middle_name") val middle_name : String,
    @SerializedName("last_name") val last_name : String,
    @SerializedName("mobile_number") val mobile_number : String,
    @SerializedName("id_number") val id_number : String,
    @SerializedName("push_notifications_on") val push_notifications_on : Boolean
)