package com.supernal.freehit.Models

import com.google.gson.annotations.SerializedName

data class Mycouponsresponse (

    @SerializedName("count") val count : Int,
    @SerializedName("next") val next : String,
    @SerializedName("previous") val previous : String,
    @SerializedName("results") val results : List<Mycouponsresult>
)