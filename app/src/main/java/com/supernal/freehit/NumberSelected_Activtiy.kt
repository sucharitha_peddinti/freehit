package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.supernal.freehit.Utility.LottoTicket
import java.util.*
import kotlin.collections.ArrayList


class NumberSelected_Activtiy : Activity() {

    lateinit var gridViewnumselect: GridView
    var selctedlist: ArrayList<Int> = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.numberselection_layout)

        val next_button = findViewById<ImageView>(R.id.next_button)
        gridViewnumselect = findViewById<GridView>(R.id.gridViewnumselect) as GridView

        next_button.setOnClickListener {
            intent = Intent(applicationContext, Coupns_Activity::class.java)
            startActivity(intent)
            finish()
        }
        val ticket = Utility.createLottoTicket()
        Utility.renderTicketIntoViewGroup(
            ticket, findViewById<View>(R.id.lottoTicket_constellation_normal) as ViewGroup,
            findViewById<View>(R.id.lottoTicket_constellation_special) as ViewGroup
        )
        createLottoTicket()


        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val gson = Gson()
        val json = sharedPrefs.getString("picknumbers", "")
        val type =
            object : TypeToken<ArrayList<Int?>?>() {}.type


//        val arrayList: ArrayList<Int> = gson.fromJson(json, type)

        Log.e("picknu selected", "" + type)
        Log.e("json selected", "" + json)



    }

    companion object {
        private const val lottoPool = 70
        fun createLottoTicket(): LottoTicket { // Create a empty millionaire list
            val millionaire = ArrayList<Int?>(8)
            // Define ArrayList to hold Integer objects
            val numbers = ArrayList<Int?>()
            for (i in 0 until lottoPool) {
                numbers.add(i + 1)
            }
            Collections.shuffle(numbers)
            for (j in 0..7) {
                millionaire.add(numbers[j])
            }
            val specialNumber = millionaire
                .removeAt(millionaire.size - 1)
            return LottoTicket(millionaire, specialNumber)

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    class ImageAdapter1(
        private val mContext: Context,
        var selectednum: ArrayList<Int>
    ) :
        BaseAdapter() {
        override fun getCount(): Int {
            return selectednum.size
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(
            position: Int,
            convertView: View,
            parent: ViewGroup
        ): View {
            var view = convertView
            //            if (view == null) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.selectednum_layout, parent, false)
            //            }
            val text = view.findViewById<TextView>(R.id.textView2)
            text.text = "" + selectednum[position]
            return view
        }

    }
}