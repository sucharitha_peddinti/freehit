package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.supernal.freehit.Api.RetrofitClient
import com.supernal.freehit.Models.User
import retrofit2.Call
import retrofit2.Response

class Bussiness_partner_activity : Activity() {
    lateinit var bussissname_string: String
    lateinit var bussissname_Edittext: EditText
    lateinit var bussiness_mobilenumber_Edittext: EditText
    lateinit var bussiness_mobilenumber_string: String
    lateinit var mobile_int: String
    lateinit var businesspartnerloginsharedpreference: SharedPreferences
    lateinit var sharedPreferences: SharedPreferences
    lateinit var loginsharedpreference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bussiness_partner_login)

        loginsharedpreference = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        businesspartnerloginsharedpreference =
            getSharedPreferences("bussinessloginpreference", Context.MODE_PRIVATE)

        bussissname_Edittext = findViewById<EditText>(R.id.bussiness_name_Edittext)
        bussiness_mobilenumber_Edittext =
            findViewById<EditText>(R.id.bussiness_mobilenumber_Edittext)
        val Busssinesenter_button = findViewById<ImageView>(R.id.Busssinesenter_button)

        Busssinesenter_button.setOnClickListener {

            bussissname_string = bussissname_Edittext.text.toString().trim()
            bussiness_mobilenumber_string = bussiness_mobilenumber_Edittext.text.toString().trim()

            if (bussissname_string.isEmpty()) {
                bussissname_Edittext.error = "Please enter your name"
                bussissname_Edittext.requestFocus()
            } else if (bussiness_mobilenumber_string.isEmpty() || bussiness_mobilenumber_string.length < 10) {
                bussiness_mobilenumber_Edittext.error = "Please enter valid mobile number"
                bussiness_mobilenumber_Edittext.requestFocus()
            } else {

                postbussinesuserApi()

            }
        }
    }

    //user signup api integration


    //user signup api integration
    private fun postbussinesuserApi() {


        mobile_int = bussiness_mobilenumber_Edittext.text.toString().trim()


        val call = RetrofitClient
            .instance
            .bussinesscreateUser(
                mobile_int.toLong()
            )


        Log.e("mobile num", "" + mobile_int)


        call.enqueue(object : retrofit2.Callback<User> {


            override fun onResponse(
                call: Call<User>,
                response: Response<User>
            ) {

                if (response.isSuccessful) {

                    val dr = response.body()
                    Toast.makeText(
                        this@Bussiness_partner_activity,
                        "Login Sucess",
                        Toast.LENGTH_LONG
                    ).show()

                    val editor = loginsharedpreference.edit()

                    var luckynumbers = response.body()?.number

                    var token = response.body()?.token
                    Log.e("token", token)
                    editor.putString("token", token)
                    editor.putBoolean("bussinesLogin", true)

                    editor.commit()

                    intent = Intent(applicationContext, Registered_Users_activity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(
                        this@Bussiness_partner_activity,
                        "Registration Failed ",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
            }
        })
    }


    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}