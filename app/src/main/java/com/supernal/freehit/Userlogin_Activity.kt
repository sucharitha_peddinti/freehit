package com.supernal.freehit

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.supernal.freehit.Api.RetrofitClient
import com.supernal.freehit.Models.User
import retrofit2.Call
import retrofit2.Response


class Userlogin_Activity : AppCompatActivity() {
    lateinit var loginsharedpreference: SharedPreferences
    lateinit var businesspartnerloginsharedpreference: SharedPreferences
    var mobile_long: Long = 0
    lateinit var enter_Button: Button
    lateinit var mobile: EditText
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        loginsharedpreference = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)
        businesspartnerloginsharedpreference =
            getSharedPreferences("bussinessloginpreference", Context.MODE_PRIVATE)

        enter_Button = findViewById<Button>(R.id.Enter_Button)
        val Bussiness_partner_Textview = findViewById<TextView>(R.id.Bussiness_partner_Textview)
        mobile = findViewById<EditText>(R.id.editmobile) as EditText

        if (loginsharedpreference.getBoolean("islogined", false)) {

            intent = Intent(this, Coupns_Activity::class.java)
            startActivity(intent)
            finish()
        }

//        if (loginsharedpreference.getBoolean("bussinesLogin", false)) {
//
//            intent = Intent(this, Registered_Users_activity::class.java)
//            startActivity(intent)
//            finish()
//        }
        enter_Button.setOnClickListener {


            if (mobile.text.toString().isEmpty() || mobile.text.toString().length < 10) {
                mobile.error = "Enter a valid mobile number"
                mobile.requestFocus()
            } else {
                postuserApi()
            }

        }
        Bussiness_partner_Textview.setOnClickListener {
            intent = Intent(applicationContext, Bussiness_partner_activity::class.java)
            startActivity(intent)

        }

    }

    //user signup api integration
    private fun postuserApi() {


        mobile_long = mobile.text.toString().trim().toLong()


        val call = RetrofitClient
            .instance
            .createUser(
                mobile_long
            )


        Log.e("mobile num", "" + mobile_long)


        call.enqueue(object : retrofit2.Callback<User> {


            override fun onResponse(
                call: Call<User>,
                response: Response<User>
            ) {

                if (response.isSuccessful) {

                    val dr = response.body()
                    Log.e("response", "" + response.toString())


                    sharedPreferences =
                        getSharedPreferences(
                            "loginprefs",
                            Context.MODE_PRIVATE
                        )
                    val editor = sharedPreferences.edit()

                    var token = response.body()?.token
                    var mobilenumber = response.body()?.mobile_number
                    Log.e("token", token)
                    editor.putString("token", token)
                    editor.putString("mobilenumber", mobilenumber)
                    editor.commit()



                    intent = Intent(this@Userlogin_Activity, Otp_activity::class.java)
                    intent.putExtra("mobile", mobile_long.toString())
                    startActivity(intent)

                    finish()


                } else {

                    Toast.makeText(
                        this@Userlogin_Activity,
                        "" + response.message(),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }

            }

            override fun onFailure(call: Call<User>, t: Throwable) {

            }
        })


    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}

