package com.supernal.freehit

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.supernal.freehit.Api.APIClient
import com.supernal.freehit.Api.Api
import com.supernal.freehit.Models.Mycouponsresponse
import com.supernal.freehit.Models.Mycouponsresult
import com.supernal.freehit.adapters.Coupons_adapter
import com.supernal.freehit.util.NetWorkConection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Coupns_Activity : Activity() {
    var doubleBackToExitOnce: Boolean = false
    lateinit var couponsrecycleview: RecyclerView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var progressBarcoupns: ProgressBar
    lateinit var  mycoupsAdapter: Coupons_adapter
    lateinit var nocoupons_layout:LinearLayout


    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.your_coupons)
        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        couponsrecycleview = findViewById<RecyclerView>(R.id.couponsrecycleview)
        progressBarcoupns = findViewById<ProgressBar>(R.id.progressBarcoupns)
        nocoupons_layout = findViewById<LinearLayout>(R.id.nocoupons_layout)


        val how_to_play_logo = findViewById<ImageView>(R.id.how_to_play_logo)
        val wincup_logo = findViewById<ImageView>(R.id.wincup_logo)
//        val picknumbers = findViewById<ImageView>(R.id.picknumbers)
        couponsrecycleview.layoutManager =
            LinearLayoutManager(applicationContext)


        how_to_play_logo.setOnClickListener {
            intent = Intent(applicationContext, How_to_play_activity::class.java)
            startActivity(intent)
            finish()

        }

        wincup_logo.setOnClickListener {
            intent = Intent(applicationContext, Status_Activity::class.java)
            startActivity(intent)
            finish()
        }

        getmycoupons()

    }

    private fun getmycoupons() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            try {


                //Set the Adapter to the RecyclerView//

                var token = sharedPreferences.getString("token", "")

                var apiServices = APIClient.client.create(Api::class.java)
                val call = apiServices.getcoupon("token " + token)

                call.enqueue(object : Callback<Mycouponsresponse> {
                    override fun onResponse(
                        call: Call<Mycouponsresponse>,
                        response: Response<Mycouponsresponse>
                    ) {
                        Log.e("mycoups response", response.body()!!.results.toString())

                        progressBarcoupns.visibility = View.GONE

                        if (response.isSuccessful) {

                            try {

                                val listOfCoupons: List<Mycouponsresult> =
                                    response.body()?.results!!

                                if (listOfCoupons.isEmpty()){
                                    nocoupons_layout.visibility=View.VISIBLE
                                    couponsrecycleview.visibility=View.GONE

                                }else
                                {
                                    nocoupons_layout.visibility=View.GONE
                                    couponsrecycleview.visibility=View.VISIBLE


//                                var num = listOfCoupons.get(0).number


                                //Set the Adapter to the RecyclerView//

                                Log.e("mycoups response", response.body()!!.results.toString())


                                mycoupsAdapter =

                                    Coupons_adapter(applicationContext, listOfCoupons)

                                couponsrecycleview.adapter = mycoupsAdapter

                                couponsrecycleview.addOnItemClickListener(object :
                                    OnItemClickListener {
                                    override fun onItemClicked(position: Int, view: View) {
                                        // Your logic

                                        var defaultnumbers=mycoupsAdapter.list.get(position).number

                                        Log.e("Defaultnumbers",defaultnumbers)
                                        var editor =sharedPreferences.edit()
                                        editor.putString("expirydate",mycoupsAdapter.list.get(position).scratch_date)
                                        editor.putString("defaultnumbers",defaultnumbers)
                                        editor.commit()

                                        Log.e("scratch date",""+mycoupsAdapter.list.get(position).scratch_date)
                                        intent = Intent(
                                            applicationContext,
                                            Defaultcouponnumbers_Activity::class.java
                                        )
                                        startActivity(intent)

                                    }

                                })
                                }
                            } catch (ex: NumberFormatException) { // handle your exception
                                ex.printStackTrace()
                            }

                        }
                    }

                    override fun onFailure(call: Call<Mycouponsresponse>?, t: Throwable?) {
                        progressBarcoupns.visibility = View.GONE
                        Log.e("mycoups", t.toString())
                    }
                })

            } catch (e: NumberFormatException) {
                e.printStackTrace()

            }
        } else {

            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
        }

    }


    //on item click interface
    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
            }


            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }


    override fun onBackPressed() {


        if (doubleBackToExitOnce) {
            super.onBackPressed()
            return
            finish()

        }

        this.doubleBackToExitOnce = true
        //displays a toast message when user clicks exit button
        Toast.makeText(applicationContext, "please press again to exit ", Toast.LENGTH_LONG).show()

        //displays the toast message for a while
        Handler().postDelayed({
            kotlin.run {

                doubleBackToExitOnce = false

                finish()
            }
        }, 5000)


    }
}
