package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class Otp_activity : Activity() {
lateinit var progressBar:ProgressBar
 lateinit var loginsharedpreference:SharedPreferences
    //These are the objects needed
//It is the verification id that will be sent to the user
    private lateinit var mVerificationId: String
    //The edittext to input the code
    private lateinit var Otpedit: EditText
    private lateinit var Go_Button: Button
    //firebase auth object`
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.otp_activity)

        loginsharedpreference=getSharedPreferences("loginprefs",Context.MODE_PRIVATE)
        //initializing objects
        mAuth = FirebaseAuth.getInstance()
        Otpedit = findViewById<EditText>(R.id.Otpedit) as EditText
        Go_Button = findViewById<Button>(R.id.Go_Button) as Button
        progressBar=findViewById<ProgressBar>(R.id.progressBar) as ProgressBar
        //getting mobile number from the previous activity
//and sending the verification code to the number
        val intent = intent
        val mobile = intent.getStringExtra("mobile")
        sendVerificationCode(mobile)


        //if the automatic sms detection did not work, user can also enter the code manually
//so adding a click listener to the button

        Go_Button.setOnClickListener {

            progressBar.visibility = View.VISIBLE

            val code = Otpedit.text.toString().trim()
            if (code.isEmpty() || code.length < 6) {
                Otpedit.error = "Enter valid code"
                Otpedit.requestFocus()



            } else {
                verifyVerificationCode(code)

            }
        }

    }

    //the method is sending verification code
//the country id is concatenated
//you can take the country id as user input as well
    private fun sendVerificationCode(mobile: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+91$mobile",
            60,
            TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD,
            mCallbacks
        )
    }


    //the callback to detect the verification status
    private val mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

                //Getting the code sent by SMS
                val code = phoneAuthCredential.smsCode
                //sometime the code is not detected automatically
//in this case the code will be null
//so user has to manually enter the code
                if (code != null) {
                    Otpedit.setText(code)
                    //verifying the code
                    verifyVerificationCode(code)
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(this@Otp_activity, e.message, Toast.LENGTH_LONG).show()
            }

            override fun onCodeSent(
                s: String,
                forceResendingToken: PhoneAuthProvider.ForceResendingToken
            ) {
                super.onCodeSent(s, forceResendingToken)
                //storing the verification id that is sent to the user
                mVerificationId = s
            }
        }

    private fun verifyVerificationCode(code: String) { //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId, code)
        //signing the user
        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(
                this@Otp_activity
            ) { task ->
                if (task.isSuccessful) { //verification successful we will start the profile activity
                    val intent =
                        Intent(this@Otp_activity, User_details::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)

                    var editor=loginsharedpreference.edit()
                    editor.putBoolean("islogined",true)
                    editor.commit()
                } else { //verification unsuccessful.. display an error message
                    var message =
                        "Somthing is wrong, we will fix it soon..."
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        message = "Invalid code entered..."
                }

                    Toast.makeText(this, "Please enter vaild otp", Toast.LENGTH_LONG).show()

                }
            }
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}