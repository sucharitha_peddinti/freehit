package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import com.google.gson.Gson
import com.supernal.freehit.Api.RetrofitClient
import com.supernal.freehit.Models.Luckynumcoupons_Model
import retrofit2.Call
import retrofit2.Response
import java.util.*

class NumberSeletion_Activity : Activity(), OnItemClickListener,
    View.OnClickListener {
    lateinit var sharedPreferences: SharedPreferences

    lateinit var mNumberGridView: GridView
    lateinit var gridViewselectedNumbers: GridView
    lateinit var Pickforme: Button
    lateinit var submitnumbers: Button
    lateinit var picknumberslayout: RelativeLayout

    private val chosedNumbers: ArrayList<Int> = ArrayList<Int>()
    var textview_1: TextView? = null
    var textview_2: TextView? = null
    var textview_3: TextView? = null
    var textview_4: TextView? = null
    var textview_5: TextView? = null
    var textview_6: TextView? = null
    var textview_7: TextView? = null
    var textview_8: TextView? = null
    lateinit var seletedjson: String
    var selectednum = ArrayList<Int>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.numberchoose_layout)
        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        val how_to_play_logo = findViewById<ImageView>(R.id.how_to_play_logo)
        val expirydate = findViewById<TextView>(R.id.expirydate)
        how_to_play_logo.setOnClickListener {
            intent = Intent(applicationContext, How_to_play_activity::class.java)
            startActivity(intent)
            finish()
        }
        var expirydate_String=sharedPreferences.getString("expirydate","")
        expirydate.text="THIS OFFER CLOSES ON "+expirydate_String


        mNumberGridView = findViewById(R.id.gridViewLottoNumber)
        gridViewselectedNumbers = findViewById(R.id.gridViewselectedNumbers)
        picknumberslayout = findViewById(R.id.picknumberslayout)
        Pickforme = findViewById(R.id.Pickforme)
        submitnumbers = findViewById(R.id.submitnumbers)
        // set grid view adapter
        mNumberGridView.adapter = ImageAdapter(this)
        mNumberGridView.onItemClickListener = this


        Pickforme.setOnClickListener(View.OnClickListener {


            val ticket = Utility.createLottoTicket()
            Utility.renderTicketIntoViewGroup(
                ticket,
                findViewById<LinearLayout>(R.id.lottoTicket_constellation_normal) as ViewGroup,
                findViewById<LinearLayout>(R.id.lottoTicket_constellation_special) as ViewGroup
            )
            createLottoTicket()
            picknumberslayout.visibility = View.VISIBLE
            gridViewselectedNumbers.visibility = View.GONE


        })
        submitnumbers.setOnClickListener(View.OnClickListener {
            picknumberslayout.visibility = View.GONE
            gridViewselectedNumbers.visibility = View.VISIBLE
            if (selectednum.size < 8) {
                Toast.makeText(
                    this@NumberSeletion_Activity,
                    "Please Select  8  Numbers..",
                    Toast.LENGTH_LONG
                ).show()
            } else {


                postluckynumbers(selectednum.toString())
            }
        })


    }

    companion object {
        private const val lottoPool = 70
        fun createLottoTicket(): Utility.LottoTicket { // Create a empty millionaire list
            val millionaire = ArrayList<Int?>(8)
            // Define ArrayList to hold Integer objects
            val numbers = ArrayList<Int?>()
            for (i in 0 until lottoPool) {
                numbers.add(i + 1)
            }
            Collections.shuffle(numbers)
            for (j in 0..7) {
                millionaire.add(numbers[j])

            }
            val specialNumber = millionaire
                .removeAt(millionaire.size - 1)

            return Utility.LottoTicket(millionaire, specialNumber)

        }
    }

    override fun onClick(view: View) {


        sendLottoResult()
    }

    private fun sendLottoResult() { // get current selected number
        val it = Intent()
        if (chosedNumbers.size == 8) { // have select 8 digit
            var returnString = ""
            for (j in chosedNumbers.indices) {
                returnString += chosedNumbers[j].toString()
                if (j + 1 == chosedNumbers.size) {

                    break
                }
                returnString += ","
            }
            it.putExtra("lotto", returnString)


            setResult(RESULT_OK, it)
        } else { // not yet
            setResult(RESULT_CANCELED, it)
        }
        finish()
    }

    override fun onItemClick(
        adapterView: AdapterView<*>?,
        view: View,
        i: Int,
        l: Long
    ) {
        val number = i + 1
        // check if this ball have been pressed or not
        if (chosedNumbers.contains(number)) { // Toast.makeText(this, R.string.string_number_chooser_warn_repeate,
// Toast.LENGTH_SHORT).show();
            return
        }
        Log.e("choose num", "" + chosedNumbers)
        selectednum = chosedNumbers
        Log.e("choose num", "" + chosedNumbers)
        gridViewselectedNumbers.adapter = ImageAdapter1(
            this,
            chosedNumbers
        )
        Log.e("selectednum", "" + selectednum)
        if (chosedNumbers.size < 8) {
            chosedNumbers.add(number)
            (view as ImageView).setAlpha(30)
        } else {
            Toast.makeText(this, "You can Select Only 8 Numbers... ", Toast.LENGTH_LONG).show()
        }
    }

    inner class ImageAdapter(private val mContext: Context) : BaseAdapter() {
        private val mThumbsIds = arrayOf(
            R.drawable.ic_n1, R.drawable.ic_n2,
            R.drawable.ic_n3, R.drawable.ic_n4, R.drawable.ic_n5, R.drawable.ic_n6,
            R.drawable.ic_n7, R.drawable.ic_n8, R.drawable.ic_n9, R.drawable.ic_n10,
            R.drawable.ic_n11, R.drawable.ic_n12, R.drawable.ic_n13, R.drawable.ic_n14,
            R.drawable.ic_n15, R.drawable.ic_n16, R.drawable.ic_n17, R.drawable.ic_n18,
            R.drawable.ic_n19, R.drawable.ic_n20, R.drawable.ic_n21, R.drawable.ic_n22,
            R.drawable.ic_n23, R.drawable.ic_n24, R.drawable.ic_n25, R.drawable.ic_n26,
            R.drawable.ic_n27, R.drawable.ic_n28, R.drawable.ic_n29, R.drawable.ic_n30,
            R.drawable.ic_n31, R.drawable.ic_n32, R.drawable.ic_n33, R.drawable.ic_n34,
            R.drawable.ic_n35, R.drawable.ic_n36, R.drawable.ic_n37, R.drawable.ic_n38,
            R.drawable.ic_n39, R.drawable.ic_n40, R.drawable.ic_n41, R.drawable.ic_n42,
            R.drawable.ic_n43, R.drawable.ic_n44, R.drawable.ic_n45, R.drawable.ic_n46,
            R.drawable.ic_n47, R.drawable.ic_n48, R.drawable.ic_n49, R.drawable.ic_n50,
            R.drawable.ic_n51, R.drawable.ic_n52, R.drawable.ic_n53, R.drawable.ic_n54,
            R.drawable.ic_n55, R.drawable.ic_n56, R.drawable.ic_n57, R.drawable.ic_n58,
            R.drawable.ic_n59, R.drawable.ic_n60, R.drawable.ic_n61, R.drawable.ic_n62,
            R.drawable.ic_n63, R.drawable.ic_n64, R.drawable.ic_n65, R.drawable.ic_n66,
            R.drawable.ic_n67, R.drawable.ic_n68, R.drawable.ic_n69, R.drawable.ic_n70
        )

        override fun getCount(): Int {
            return mThumbsIds.size
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            val imageView: ImageView
            if (convertView == null) {
                imageView = ImageView(mContext)
                imageView.layoutParams = AbsListView.LayoutParams(90, 90)
                imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                imageView.setPadding(10, 10, 10, 10)
            } else {
                imageView = convertView as ImageView
            }
            imageView.setImageResource(mThumbsIds[position])
            return imageView
        }

    }

    inner class ImageAdapter1(
        private val mContext: Context,
        var selectednum: ArrayList<Int>
    ) : BaseAdapter() {
        override fun getCount(): Int {
            return selectednum.size
        }

        override fun getItem(position: Int): Any? {
            return null
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            var view = convertView
            //            if (view == null) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.selectednum_layout, parent, false)
            //            }
            val text = view.findViewById<TextView>(R.id.textView2)
            text.text = "" + selectednum[position]
            val sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(applicationContext)
            val editor = sharedPrefs.edit()
            val gson = Gson()
            seletedjson = gson.toJson(selectednum[position])
            editor.putString("picknumbers", seletedjson)
            editor.commit()
            Log.e("picknumm selection", "" + seletedjson)
            return view
        }
    }

    private fun postluckynumbers(numbers: String) {


        var token = sharedPreferences.getString("token", "")


        val call = RetrofitClient
            .instance
            .sendluckynumbers(
                "token " + token,
                numbers
            )

        Log.e("lucky num", "" + numbers)


        call.enqueue(object : retrofit2.Callback<Luckynumcoupons_Model> {


            override fun onResponse(
                call: Call<Luckynumcoupons_Model>,
                response: Response<Luckynumcoupons_Model>
            ) {

                if (response.isSuccessful) {

                    val dr = response.body()
                    Log.e("response", "" + response.toString())


                    startActivity(
                        Intent(
                            this@NumberSeletion_Activity,
                            NumberSelected_Activtiy::class.java
                        )
                    )
                    finish()

                } else {

                    Toast.makeText(
                        applicationContext,
                        "" + response.message(),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }

            }

            override fun onFailure(call: Call<Luckynumcoupons_Model>, t: Throwable) {

            }
        })


    }

    //
//    private fun postrandomnumbers() {
//
//
//        var token = sharedPreferences.getString("token", "")
//
//
//        val call = RetrofitClient
//            .instance
//            .sendluckynumbers(
//                "token " + token,
//                numbers
//            )
//
//        Log.e("lucky num", "" + numbers)
//
//
//        call.enqueue(object : retrofit2.Callback<Luckynumcoupons_Model> {
//
//
//            override fun onResponse(
//                call: Call<Luckynumcoupons_Model>,
//                response: Response<Luckynumcoupons_Model>
//            ) {
//
//                if (response.isSuccessful) {
//
//                    val dr = response.body()
//                    Log.e("response", "" + response.toString())
//
//
//                    startActivity(
//                        Intent(
//                            this@NumberSeletion_Activity,
//                            NumberSelected_Activtiy::class.java
//                        )
//                    )
//                    finish()
//
//                } else {
//
//                    Toast.makeText(
//                        applicationContext,
//                        "" + response.message(),
//                        Toast.LENGTH_LONG
//                    )
//                        .show()
//                }
//
//            }
//
//            override fun onFailure(call: Call<Luckynumcoupons_Model>, t: Throwable) {
//
//            }
//        })
//
//
//    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}