package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.supernal.freehit.Api.RetrofitClient
import com.supernal.freehit.Models.SendCoupons_Model
import retrofit2.Call
import retrofit2.Response


class Registered_Users_activity : Activity() {
    lateinit var usermobile_edittext: EditText
    lateinit var sendnumber_button: Button
    var doubleBackToExitOnce: Boolean = false
    lateinit var mobilenumber: String
    lateinit var reservrecyclerView: RecyclerView
    lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.regsiterd_users)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        reservrecyclerView = findViewById(R.id.usersrecycleview) as RecyclerView
        usermobile_edittext = findViewById(R.id.usermobile_edittext) as EditText

        sendnumber_button = findViewById(R.id.sendnumber_button) as Button
        sendnumber_button.setOnClickListener {


            if (usermobile_edittext.text.toString().isEmpty() || usermobile_edittext.text.toString().length < 10) {
                usermobile_edittext.error = "Enter a valid mobile number"
                usermobile_edittext.requestFocus()
            } else {
                sendcoupon()

            }
        }
        val personNames = arrayOf(
            "January",
            "Febuary",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        )
        val spinner = findViewById<Spinner>(R.id.spinner)
        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this, R.layout.spinner_adapter, personNames)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    Toast.makeText(
                        this@Registered_Users_activity,
                        " " + personNames[position],
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }
    }

    private fun sendcoupon() {



        var token = sharedPreferences.getString("token", "")

        mobilenumber =usermobile_edittext.text.toString().trim()

        val call = RetrofitClient
            .instance
            .sendcoupon(
                "token " + token,
                mobilenumber.toLong()
            )

        Log.e("mobile num", "" + mobilenumber)

        call.enqueue(object : retrofit2.Callback<SendCoupons_Model> {

            override fun onResponse(
                call: Call<SendCoupons_Model>,
                response: Response<SendCoupons_Model>
            ) {

                if (response.isSuccessful) {

                    val dr = response.body()
                    Log.e("response", "" + response.toString())
                    val editor = sharedPreferences.edit()


                    var number = response.body()?.number
                    Log.e("number", number)
                    editor.putString("luckynumbers", number)
                    editor.commit()


                    val sendIntent = Intent()
                    sendIntent.action = Intent.ACTION_SEND
                    sendIntent.putExtra(
                        Intent.EXTRA_TEXT,
                        "Hey, FREE HIT gives you a wonderful chance to trail your luck, CLICK HERE " +number
                    )

                    Toast.makeText(applicationContext, "coupon sent", Toast.LENGTH_LONG).show()

                    sendIntent.type = "text/plain"
                    startActivity(sendIntent)
                    finish()

                } else {

                    Toast.makeText(
                        applicationContext,
                        "" + response.message(),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }

            }

            override fun onFailure(call: Call<SendCoupons_Model>, t: Throwable) {

            }
        })

    }

    override fun onBackPressed() {


        if (doubleBackToExitOnce) {
            super.onBackPressed()
            return
            finish()

        }

        this.doubleBackToExitOnce = true
        //displays a toast message when user clicks exit button
        Toast.makeText(applicationContext, "please press again to exit ", Toast.LENGTH_LONG).show()

        //displays the toast message for a while
        Handler().postDelayed({
            kotlin.run {
                doubleBackToExitOnce = false

                finish()
            }
        }, 5000)


    }
}

