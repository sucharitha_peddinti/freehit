package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class User_details:Activity() {

    lateinit var name_strng:String
    lateinit var mobilenumber_strng:String
    lateinit var luckynumber_strng:String
    lateinit var loginsharedpreference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_details)

        val name_Edittext = findViewById<EditText>(R.id.name_Edittext)
        val mobilenumber_Edittext = findViewById<EditText>(R.id.mobilenumber_Edittext)
        val luckynumber_Edittext = findViewById<TextView>(R.id.luckynumber_Edittext)

        val userdetails_enterbutton = findViewById<FrameLayout>(R.id.userdetails_enterbutton)
        loginsharedpreference=getSharedPreferences("loginprefs",Context.MODE_PRIVATE)


        if (loginsharedpreference.getBoolean("islogined",false)){

            var mobilenumber=loginsharedpreference.getString("mobilnumber","")
            var luckynumber=loginsharedpreference.getString("luckynumbers","")
            mobilenumber_Edittext.setText(mobilenumber)
            luckynumber_Edittext.setText("Your Lucky Number "+luckynumber)
        }
        userdetails_enterbutton.setOnClickListener {

            name_strng = name_Edittext.text.toString().trim()
            mobilenumber_strng = mobilenumber_Edittext.text.toString().trim()
            luckynumber_strng = luckynumber_Edittext.text.toString().trim()

            if (name_strng.isEmpty()) {
                name_Edittext.error = "please enter the Name"
                name_Edittext.requestFocus()
            }else if (mobilenumber_strng.isEmpty() || mobilenumber_strng.length<10) {
                    mobilenumber_Edittext.error = "please enter valid mobile number"
                    mobilenumber_Edittext.requestFocus()
                }else if (luckynumber_strng.isEmpty()) {
                        luckynumber_Edittext.error = "please enter your lucky number"
                        luckynumber_Edittext.requestFocus()
                    }else{
                intent = Intent(applicationContext, How_to_play_activity::class.java)
                startActivity(intent)
                finish()
            }

        }
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}