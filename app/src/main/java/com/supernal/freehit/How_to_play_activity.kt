package com.supernal.freehit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView

class How_to_play_activity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.how_to_play)

        val next_button = findViewById<ImageView>(R.id.next_button)

        next_button.setOnClickListener {
            intent = Intent(applicationContext, Coupns_Activity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        intent = Intent(applicationContext, Coupns_Activity::class.java)
        startActivity(intent)
        finish()
    }


}
