package com.supernal.freehit.adapters

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.supernal.freehit.Models.Mycouponsresponse
import com.supernal.freehit.Models.Mycouponsresult
import com.supernal.freehit.R

class Coupons_adapter(var context: Context, var list: List<Mycouponsresult>) :
    RecyclerView.Adapter<Coupons_adapter.MyViewHolder>() {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var category_ist: List<Mycouponsresponse>

    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        sharedPreferences =context.getSharedPreferences("loginprefs",Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.your_coupons_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        val coupon_id = list.get(position).coupon_id
        val scratch_date = list.get(position).scratch_date



        holder.couponid.text = coupon_id.toString()
        holder.coupondate.text = scratch_date

        Log.e("coupon_id", "" + coupon_id)

    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val couponid = itemView!!.findViewById(R.id.couponid) as TextView
        val coupondate = itemView!!.findViewById(R.id.coupondate) as TextView


    }
}