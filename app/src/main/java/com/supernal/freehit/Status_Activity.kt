package com.supernal.freehit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goibibo.libs.views.ScratchRelativeLayoutView
import com.supernal.freehit.Api.APIClient
import com.supernal.freehit.Api.Api
import com.supernal.freehit.adapters.Coupons_adapter
import com.supernal.freehit.util.NetWorkConection

class Status_Activity : Activity() {

    lateinit var scratchedcouponsrecycleview: RecyclerView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var progressBarcoupns: ProgressBar
    lateinit var mycoupsAdapter: Coupons_adapter
    lateinit var nocoupons_layout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.staus_coupons)
        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        scratchedcouponsrecycleview = findViewById<RecyclerView>(R.id.scratchedcouponsrecycleview)
        progressBarcoupns = findViewById<ProgressBar>(R.id.progressBarcoupns)
        val backarrow = findViewById<ImageView>(R.id.backarrow)

        scratchedcouponsrecycleview.layoutManager =
            LinearLayoutManager(applicationContext)


        backarrow.setOnClickListener {
            intent = Intent(applicationContext, Coupns_Activity::class.java)
            startActivity(intent)
            finish()

        }


        val scratchRelativeLayoutView =
            findViewById<ScratchRelativeLayoutView>(R.id.lytScratch)
        scratchRelativeLayoutView.setStrokeWidth(20)
        scratchRelativeLayoutView.setScratchView(R.layout.lyt_scratch)
        scratchRelativeLayoutView.setRevealListener(object :
            ScratchRelativeLayoutView.IRevealListener {
            override fun onRevealed(tv: ScratchRelativeLayoutView) { // on reveal
            }

            override fun onRevealPercentChangedListener(
                siv: ScratchRelativeLayoutView,
                percent: Float
            ) { // on percent change
            }
        })
    }

//    //Get sacrtched coupons
//
//    private fun getscratchedcoupons() {
//
//        if (NetWorkConection.isNEtworkConnected(this)) {
//
//            try {
//
//
//                //Set the Adapter to the RecyclerView//
//
//                var token = sharedPreferences.getString("token", "")
//
//                var apiServices = APIClient.client.create(Api::class.java)
//                val call = apiServices.getscratchedcoupon("token " + token)
//
//            }
//        }
//    }
//

    override fun onBackPressed() {
        super.onBackPressed()
        intent = Intent(applicationContext, Coupns_Activity::class.java)
        startActivity(intent)
        finish()
    }
}
